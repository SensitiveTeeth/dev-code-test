# oneCHARGE dev code test

oneCHARGE developer interview coding test

## Intructions

You will find few exercises to help us understanding your abilities to code. There are no limit of time, but we would like you to be transparent with us and let us know how long it took to do each section.

- Please finish your method in any programming language you are familiar with preferred Ruby / JS / Go / Python
- Try your best to finish as much as you can even you don't know the answer. If you are new to any programming languages, please explain your logic clearly to solve the problem.
- Put your answers in a git repo and share the link with us, each answer should be in its own execuable file.

We want to see how you use best practices to solve the problem and follow the coding convenstions of your selected languages, most importantly... clean code :)


### Question 1.

Print a staircase which takes interge N as the heigh of the staircase

__Example:__

__Input:__

```
n = 6
```

__Output:__

```
     #
    ##
   ###
  ####
 #####
######
```

---

### Question 2.

Given a non-empty, singly linked list with head node head, return a middle node of linked list.

If there are two middle nodes, return the second middle node.

__Example 1:__

```
Input: [1,2,3,4,5]
Output: Node 3 from this list (Serialization: [3,4,5])
The returned node has value 3.  (The judge's serialization of this node is [3,4,5]).

Note that we returned a ListNode object ans, such that:
ans.val = 3, ans.next.val = 4, ans.next.next.val = 5, and ans.next.next.next = NULL.
```

__Example 2:__

```
Input: [1,2,3,4,5,6]
Output: Node 4 from this list (Serialization: [4,5,6])

Since the list has two middle nodes with values 3 and 4, we return the second one.
```

Note:

The number of nodes in the given list will be between `1` and `100`.

---

### Question 3.

There are two sorted arrays `nums1` and `nums2` of size m and n respectively.

Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).

You may assume `nums1` and `nums2` cannot be both empty.

__Example 1:__

```
nums1 = [1, 3]
nums2 = [2]

The median is 2.0
```

__Example 2:__

```
nums1 = [1, 2]
nums2 = [3, 4]

The median is (2 + 3)/2 = 2.5
```
